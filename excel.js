//Generate the class
//IE11 doesnt support the class
class Cell{
    constructor(col, row, val, type){
        this.col = col;
        this.row = row;
        this.val = val;
        this.type = type;
    }

    get col(){ return this._col; }
    set col(colval){ this._col = colval; }

    get row(){ return this._row; }
    set row(rowval){ this._row = rowval; }

    get val(){ return this._val; }
    set val(valval){ this._val = valval; }

    get type(){ return this._type; }
    set type(typeval){ this._type = typeval; }
}

//declare the table
const TBLROWS = 20;
const TBLCOLS = 10;

//helper function to get document.getElementById
function $gel(id){
    return document.getElementById(id);
}

//table ready onload function 
function tblReady(){
    drawTable();
}

//dynamic function to draw the table
function drawTable(){
    //declare table
    var tblDiv = "<table border ='1' cellpadding = '0' cellspacing='0' class='TableClass' id='tbl'>";

    //column header label
    tblDiv += "<tr><th></th>";

    //row header A to J
    for(var i = 0; i < TBLCOLS; i++){
        tblDiv += "<th>" + String.fromCharCode(i+65) + "</th>";
    }

    //close the row header
    tblDiv += "</tr>";

    //main table
    for(var i = 0; i < TBLROWS; i++){
        tblDiv += "<tr>";

        tblDiv += "<td id='" + (i+1) + "_0' class='colHeader'>" + (i+1) + "</td>";
        
        //main cells to take the input
        for(var j = 0; j < TBLCOLS; j++){
            var key = (i+1) + "_" + String.fromCharCode(j+65);
            var cell = inputMap.get(key);
            var value = getVal(cell);
            
            //var value = getVal(inputMap.get((i+1)+(String.fromCharCode(j+65))));
            tblDiv += "<td id='" + (i+1) + "_" + String.fromCharCode(j+65) + "' class='mainCol' onclick='selected(this)'>" + value  + "</td>"
        }

        tblDiv += "</tr>";
    }

    //close table
    tblDiv += "</table>";

    // //display table in proper div
    $gel("tblExcel").innerHTML = tblDiv;

    return tblDiv;
}

// find out type of browsers that is available
var isIE11 = ((window.navigator.userAgent).indexOf("Trident") !== -1);
var isChrome = ((window.navigator.userAgent).indexOf("Chrome") !== -1);
var isSafari = ((window.navigator.userAgent).indexOf("Safari") !== -1);

//declare the Map to store the key and value for the cellId and inputtext 
var inputMap = new Map();

var cell;

//check the value is whether formula or text or number
function checkType(val){
    var text = /^[a-zA-Z ]*$/;//regular expression
    //fomula example to extract =SUM
    var sumSyntax = "=SUM(Ax:By)";
    //Detect first four letter of formula and define it as a formula
    //formula will be return =SUM 
    var formula=sumSyntax.substr(0,4);

    if(val.substr(0,4).toUpperCase() === formula){
        console.log(val.substr(0,4).toUpperCase());
        return "Formula";
    }
    else if(text.test(val) === true){
        return "Text";
    }
    else{
        return "Number";
    }
}

//onclick event for the cell
window.selected = function(ref){
    
    //get Excel type of cell selection
    $gel(ref.id).style.backgroundColor = "lightblue";

    //split and get the column and row to display in selected cell indicate box 
    var cellIdPart = ref.id.split('_');
    console.log(ref.id);
    console.log(cellIdPart);    

    //display A1, A2 type of cell indication in cellSel textbox
    $gel("cellSel").value = cellIdPart[1] + cellIdPart[0]; 

    //store the id in the variable
    var cellId = ref.id;
    console.log(cellId);
    $gel("txtInput").focus();
    //boost the onkeypress event when cell has been selected
    window.inputvalue = function(){ 
        //this.$gel(ref.id).style.backgroundColor = "lightblue";  
        if(isIE11){
            if (window.event.keyCode === 13) {
                console.log(ref.id);
                //var cellIdPart = ref.id.split('_');
                
                //get the text box input value
                var newVal = $gel("txtInput").value;
                console.log(newVal);

                var type = checkType(newVal);
                //set the cellId as a key and newVal as a value in inputMap
                cell = new Cell(cellIdPart[1], cellIdPart[0], newVal, type);

                console.log(cell);
                inputMap.set(cellId, cell);
                console.log(inputMap);

                //After input change the background color of selected cell to white again
                $gel(cellId).style.backgroundColor = "white";

                $gel("txtInput").value="";
               
                drawTable();
            }
        }      
        else {
            // Chrome, Edge and Safari use returnValue
            if (window.event.keyCode === 13) {
                console.log(ref.id);
                //var cellIdPart = ref.id.split('_');
                
                //get the text box input value
                var newVal = $gel("txtInput").value;
                console.log(newVal);

                var type = checkType(newVal);
                //set the cellId as a key and newVal as a value in inputMap
                cell = new Cell(cellIdPart[1], cellIdPart[0], newVal, type);

                console.log(cell);
                inputMap.set(cellId, cell);
                console.log(inputMap);

                //After input change the background color of selected cell to white again
                $gel(cellId).style.backgroundColor = "white";

                $gel("txtInput").value="";
               
                drawTable();
            }
            
        }  
    }
    
}

//to check and calcualte based on cell type
function getVal(input){
    if(input == undefined){
        return "";
    }
    if (input._type === "Number" || input._type === "Text"){
        console.log(input._val);

        return input._val;
    }
    else if(input._type === "Formula"){
        //=SUM(Ax:By)
        //detect pattern of ( : )
        var pattern = /[:|\(|\)]/;
        //get three different part 1) =sum   2) Ax 3) By
        var detectFormula = input._val.split(pattern);

        if (input._val !== null){
            //starting column : Ax:By -> A
            var fromColumn = detectFormula[1].substr(0, 1);
            //ENDING column : Ax:By -> B
            var toColumn = detectFormula[2].substr(0, 1);

            //starting Row : Ax:By -> x
            var fromRow = detectFormula[1].substr(1, detectFormula[1].length - 1);

            //ending Row : Ax:By -> x
            var toRow = detectFormula[2].substr(1, detectFormula[2].length - 1);

            console.log(fromColumn + "\n" + toColumn);
            console.log(fromRow + "\n" + toRow);

            //column as letters -> need to change to numbers
            fromColumn = fromColumn.charCodeAt(0) - 65 +1;          
            toColumn =  toColumn.charCodeAt(0) - 65 +1;  

            toRow = parseInt(toRow); 
            fromRow = parseInt(fromRow); 

            var sumTotal = 0;
            //=sum(A2:A6)
            for (var i = fromRow; i <= toRow; i++){
                for (var j = fromColumn; j <= toColumn; j++){
                    // make sure we have a number for addition
                        var col = String.fromCharCode(j+65-1);
                        var cell = inputMap.get(i + "_" + col);
                        
                        sumTotal += parseFloat(cell._val);
                }
            }
            return sumTotal;
        }
    }
    
}

//clear all the inputs in table
function clearAll() {
    location.reload();
}
